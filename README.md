# openstlinux-6.1-yocto-mickledore-mp1-v23.06.21

- `mkdir yocto-stm32mp`
- `cd yocto-stm32mp`
- `python3 ~/bin/repo init -u https://gitlab.com/OneKiwiTech/embedded/manifest-stm32mp -b mickledore -m mickledore-mp1-v23.06.21.xml`
- `python3 ~/bin/repo sync -j8`
- `source poky/oe-init-build-env`
- `cp ../meta-renesas/docs/template/conf/smarc-rzv2l/*.conf conf/`
- `bitbake core-image-weston`
